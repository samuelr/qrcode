# Android QR code reader example #

Based on a tutorial found at http://thorbek.net/online/2013/10/11/an-integrated-qr-scanner-in-android-activity/

### What is this repository for? ###

* Example of including a QR code scanner in an Android app.

### How do I get set up? ###

This repo is a full Eclipse ADT project. Copy the QRCode directory into your workspace and open the project.

### Who do I talk to? ###

* Samuel Raynor <samuel@samuelraynor.com>
* http://samuelraynor.com